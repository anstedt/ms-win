#! env python
import glob
import os
import ntpath
import collections
import argparse
import binascii
import time
import re

#
# Find files that match their sizes to shrink total list
# Take that smaller list and find crc's
# Use crc's to do final match
# Output results
#

# Find CRC of a file
def CRC32_from_file(filename):
  try:
    buf = open(filename,'rb').read()
    buf = (binascii.crc32(buf) & 0xFFFFFFFF)
    return "%08X" % buf
  except:
    return "-2"

sz_list = set() # Used first to shrink the total list of files
sz_dup = set() # List of duplicates based on the size
crc_list = set() # List of all CRC's
files_db_crc = collections.defaultdict(list) # ALL files with matching sx's, but not skipped
files_db = collections.defaultdict(list) # ALL files I found, but not skipped
files_failed = set()  # Debug, files that threw exception
files_skipped = set() # To small or no size found

# Files to ignore
ignore = set()
ignore.add("ZbThumbnail.info")
ignore.add("Thumbs.db")

count = 0
matches = 0
totalsz = 0

# Parse the command line
# The -p is for priority for deletion which means those that match this should be deleted first
# Example: git/ms-win/prune.py -d Pictures Videos /f/Pictures -p 'F:'
parser = argparse.ArgumentParser(description='Example: ./findsame.py -d /c/Users/hanst/Pictures/ /c/Users/hanst/Videos/ -p F:')
parser.add_argument('-d', nargs='+')
parser.add_argument('-p', nargs='+')
args = parser.parse_args()

# Default to current directory
if args.d == None:
  dirs = '.'
else:
  dirs = args.d

if args.p == None:
  del_priority = ""
else:
  del_priority = args.p[0]

# Now go through all the directories to find macthes based onm the size
for dir in dirs:
  for f in glob.iglob(dir + '/**/*', recursive=True):
    # Can we get a size, if not ignore this file
    try:
      sz = os.path.getsize(f)
      # totalsz = totalsz + sz
    except:
      sz = -1 # to indicate failure
      files_failed.add(f)

    filename = ntpath.basename(f)
    
    # We do not handle files with an error for the size
    # We still ignore the files we have selected
    if sz >= 0 and not filename in ignore:
      count = count + 1

      # We skip small files
      if sz > 1024:
        # If the sz is already in the sz list it is a duplicate
        if sz in sz_list:
          matches = matches + 1
          totalsz = totalsz + sz
          sz_dup.add(sz)
        else:
          # Only sz's that we have not found before
          sz_list.add(sz)

      # Add all files we are tracking, sz is the key for first stage
      files_db[sz].append(f)
    else:
      # Just for DEBUG, not used otherwise
      files_skipped.add(f)

# At this point we have files_db with ALL the files and we have sz_dup
# with all the duplicate sizes

# Use the sz_dup list to walk through the files_db to create CRC's we need
#db_count = 0
for sz in sz_dup:
  # For each entry k, sz, we get a list
  #db_count += 1
  for f in files_db.get(sz):
    # We can now do the CRC on the files that have duplicate sizes and
    # create a new list using the crc as the key
    crc = CRC32_from_file(f)
    print(".", end ="", flush=True)
    # Note this is in Linux format
    files_db_crc[crc].append((os.path.getctime(f), f, sz))

    # Th crc list so we can access all files in files_db_crc that have duplicate crcs.
    if not crc in crc_list:
      crc_list.add(crc)
    
  #if db_count > 50:
    #break

print("")
print("#! env bash")
print("")
print("# arguments:", dirs, del_priority)
print("")
print("################################################################################")
print("########################### FILES WITH MATCHES #################################")
print("################################################################################")

total = 0
for crc in crc_list:
  if len(files_db_crc.get(crc)) > 1 and crc != "-2":
    print("")
    # This crc has duplicates
    once = True # We keeo the oldest
    for f in sorted(files_db_crc.get(crc)):
    # for f in files_db_crc.get(crc):
      # Now we are walking through files with the same CRC
      # All we really need is if a crc has more tham one file
      total += f[2]
      # Assume the oldest, which is the first in the sorted list, is
      # the one we will keep

      # Any files in the path, such as F: that match are considered no
      # as high priority to preserve
      match = re.search(del_priority, f[1])

      # If this is the first match and it does not contain the
      # priority pattern we will use it as the one we want to keep
      if once and match == None:
        print("#####", f[1], " #####")
        once = False
      else:
        print("rm \"", f[1], "\"", sep='')
    # This removes the size for the file we will keep
    total -= f[2]

print("##### Total Bytes saved = {:,}".format(total), "#####")

