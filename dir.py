#! env python
import glob
import os
import ntpath
import collections
import argparse

d = 'HELLO'
print(type(d))

dirs = list()
print(type(dirs))

print("What the Hello World")
file_list = set()

count = 0
matches = 0
totalsz = 0

# Parse the command line
parser = argparse.ArgumentParser(description='Directory List')
parser.add_argument('-d', nargs='+')
args = parser.parse_args()

print(args)

if args.d == None:
    print("NONE")
    dirs = './'
else:
    dirs = args.d
    dirs[0] = dirs[0] + '/'

print("DIRS=", dirs)
# dirs = '.git/'
dirs = dirs + '/'
print("DIRS=", dirs)


for f in glob.iglob(dirs + '**/*', recursive=True):
    count = count + 1
    filename = ntpath.basename(f)
    # print(f, ntpath.basename(f))
    if filename in file_list:
        try:
            sz = os.path.getsize(f)
            totalsz = totalsz + sz
        except:
            print("Exception:", f)
        print(filename," : ", f, sz)
        matches = matches + 1
    file_list.add(filename)

# print(file_list)
print("Files:", count, matches, totalsz)
