#! env python
import glob
import os
import ntpath
import collections
import argparse

files_list = set()    # Just the file names for checking for duplicates
files_dup = set()     # Files that are duplicates
files_skipped = set() # To small or no size found
files_db = collections.defaultdict(list) # ALL files I found, but not skipped
files_failed = set()  # Debug, files that threw exception

# Files ti ignore
ignore = set()
ignore.add("ZbThumbnail.info")
ignore.add("Thumbs.db")

count = 0
matches = 0
totalsz = 0

# Parse the command line
parser = argparse.ArgumentParser(description='Example: ./findsame.py -d /c/Users/hanst/Pictures/ /c/Users/hanst/Videos/')
parser.add_argument('-d', nargs='+')
args = parser.parse_args()

# DB print(args)

if args.d == None:
  dirs = '.'
else:
  dirs = args.d

for dir in dirs:
  for f in glob.iglob(dir + '/**/*', recursive=True):
    # Can we get a size, if not ignore this file
    try:
      sz = os.path.getsize(f)
      totalsz = totalsz + sz
    except:
      sz = -1 # to indicate failure
      files_failed.add(f)

    filename = ntpath.basename(f)

    # For now we only could files creater than 1024
    if sz > 1024 and not filename in ignore:
    # if sz > 1024:
      count = count + 1

      # If the file is already in the list it is a duplicate
      if filename in files_list:
        matches = matches + 1
        files_dup.add(filename)
      else:
        # Only files that we have not found before
        files_list.add(filename)

      # Add all files we are tracking
      # 0 is a place holder for a CRC
      files_db[filename].append((f ,sz, 0))
    else:
      files_skipped.add(f)

# Only duplicate files
for k in files_dup:
  # For each entry k we get a list
  # The list shows us all the directories where a filename is duplicated
  print("---------------------------------------------------------------------------------")
  for l in files_db.get(k):
    (n, s, c) = l
    # print("Filename:", k, " Fullname:", n, " Size:", s, " CRC:", c)
    print("{:>0} {:>0} {:>20} {:>0} {:>10} {:>0} {:>10} {:>0}".format("Filename:", k, "Full-name:", n, "Size:", s, "CRC:", c))
    
# print("file_list=", files_list)
print("Files:", count, matches, "{:,}".format(totalsz))
