#! env python
import glob
import os
import ntpath
import collections
import argparse
import binascii

def CRC32_from_file(filename):
  try:
    buf = open(filename,'rb').read()
    buf = (binascii.crc32(buf) & 0xFFFFFFFF)
    return "%08X" % buf
  except:
    return "-2"

files_list = set()    # Just the file names for checking for duplicates
files_dup = set()     # Files that are duplicates
files_skipped = set() # To small or no size found
files_db = collections.defaultdict(list) # ALL files I found, but not skipped
files_failed = set()  # Debug, files that threw exception

# Files ti ignore
ignore = set()
ignore.add("ZbThumbnail.info")
ignore.add("Thumbs.db")

count = 0
matches = 0
totalsz = 0

# Parse the command line
parser = argparse.ArgumentParser(description='Example: ./findsame.py -d /c/Users/hanst/Pictures/ /c/Users/hanst/Videos/')
parser.add_argument('-d', nargs='+')
args = parser.parse_args()

# DB print(args)

if args.d == None:
  dirs = '.'
else:
  dirs = args.d

for dir in dirs:
  for f in glob.iglob(dir + '/**/*', recursive=True):
    # Can we get a size, if not ignore this file
    try:
      sz = os.path.getsize(f)
      # totalsz = totalsz + sz
    except:
      sz = -1 # to indicate failure
      files_failed.add(f)

    filename = ntpath.basename(f)

    # For now we only could files creater than 1024
    if sz > 1024 and not filename in ignore:
    # if sz > 1024:
      count = count + 1

      # If the file is already in the list it is a duplicate
      if filename in files_list:
        matches = matches + 1
        totalsz = totalsz + sz
        files_dup.add(filename)
        # print(f, CRC32_from_file(f))
      else:
        # Only files that we have not found before
        files_list.add(filename)

      # Add all files we are tracking
      # 0 is a place holder for a CRC
      files_db[filename].append((f ,sz, 0))
    else:
      files_skipped.add(f)

# fn is a file name that has duplicates
###
### Right now this is just modifying the db to add in the CRC
### This can be expanded to remove files that are not really duplicates
### Just based on their names alone. 
###
def filter_non_duplicates_by_size_and_crc(fn):
  new_list = set()
  # print("files_db.get(", k, "):", files_db.get(k))
  for l in files_db.get(k):
    (n, s, c) = l
    new_list.add((n, s, CRC32_from_file(n)))
  print("####################################################################")
  files_db.pop(k)
  files_db[k].append(new_list)

  print("AFTER files_db.get(", k, "):", files_db.get(k))
  
  return fn
  # First compare sizes and reject those that are different
  # Then compare CRCs of those that are left
  # Now return what is left
      
# Only duplicate files
for k in files_dup:
  # For each entry k we get a list
  # The list shows us all the directories where a filename is duplicated
  # filtered_list = filter_non_duplicates_by_size_and_crc(k)
  
  ###
  filter_non_duplicates_by_size_and_crc(k)
  ###
  #print("---------------------------------------------------------------------------------")
  #for l in files_db.get(k):
    #(n, s, c) = l
    ## print("Filename:", k, " Fullname:", n, " Size:", s, " CRC:", c)
    #print("{:>0} {:>0} {:>20} {:>0} {:>10} {:>0} {:>10} {:>0}".format("Filename:", k, "Full-name:", n, "Size:", s, "CRC:", c))
    
# print("file_list=", files_list)
print("Total", count, " Duplicates:", matches, " Size: {:,}".format(totalsz))
